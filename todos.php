<?php
class TodoItem {
	protected ?string $name;
	protected string $status;

	public function getName() {
		return $this->name;
	}

	public function setName(string $name) {
		$this->name = $name;
	}

	public function setStatus($status) {
		$this->status = $status;
	}

	public function getStatus() {
		return $this->status;
	}

	public static function new($name) {
		$item = new TodoItem();
		$item->setName($name);
		$item->setStatus('incomplete');

		return $item;
	}
}

class TodoList {
	private array $items = [];

	public function add(string $name) {
		array_push($this->items, TodoItem::new($name));
	}

	public function remove(int $index) {
		unset($this->items[$index]);
	}

	public function complete($index) {
		$item = $this->items[$index];
		$item->setStatus('complete');

	}

	public function getList() {
		return $this->items;
	}
}

$list = new TodoList();
$list->add('item 1');
var_dump($list->getList());

$list->complete(0);

var_dump($list->getList());